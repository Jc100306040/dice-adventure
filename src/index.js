// 主頁，最主要的繪製都在這邊

import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Main from './main/Main.js';

ReactDOM.render( <Main />, document.getElementById('main'));