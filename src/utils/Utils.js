// 各種公用的方法

class UTILS
{

    // 取得 Transition 相關的 Style Obj
    static getTransitionStyle()
    {
        if(arguments)
        {
            let transition = "";
            for(let i=0 ; i<arguments.length ; i++)
            {
                let argument = arguments[i];
                let prop      = argument[0];
                let duration  = argument[1];
                let delay     = argument[2] || "0s";
                let type      = argument[3] || "ease-out";
                transition += ( ( i != 0 ? "," : "") + 
                  prop + " " + duration + " " + type + " " + delay);
            }
            return {transition:transition};
        }
        return {};
    }

}

export default UTILS;
