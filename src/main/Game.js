// 遊戲本體

import React, { Component } from 'react';
import { route_setting } from './const/const.js';
import UTILS from '../utils/Utils.js';
import Model from './model/AdventureDataModel.js';
import DiceComp from './comp/DiceComp.js';
import ResetBtnComp from './comp/ResetBtnComp.js';
import ResultListShowBtnComp from './comp/ResultListShowBtnComp.js';
import ResultPanelComp from './comp/ResultPanelComp.js';
import ResultListComp from './comp/ResultListComp.js';


class Game extends Component 
{

    constructor(props)
    {
        super(props);

        // 初始化 Model
        Model.initData();

        this.state = {

            // 記錄當前位置(route_setting.start_index)
            curIndex : Model.getCurIndex(),

            // 紀錄當前的結果文字("")
            resultText : Model.getCurResultText(),

            // 紀錄當前骰子數字(0)
            curDiceNumber : Model.getCurDiceNumber(),

            // 是否可以點擊任何按鈕
            canClick : true,

            // 是否顯示結果清單
            isShowResultList : false,
        };

        // route_setting.contents.forEach( x => {
        //     x.text = "題目是什麼仍舊是秘密！";
        // });
    }

    // 判斷配置檔案的合法性
    isSettingLegal() {

        const contents = route_setting.contents;
        const details = route_setting.details;
        const startIndex = route_setting.start_index;

        // 總路線長度 = 路線設定長度 - 第0格 - 起始點索引
        const routeLength = details.length - 1 - startIndex;

        // 檢查總路線長度與題目長度是否一致
        if(contents.length !== routeLength) {
            console.error("Route length isn't the same as the number of contents:", routeLength, contents.length);
            return false;
        }

        return true;
    }

    // 移動
    move = (stepCount) => {

        return new Promise((resolve, reject) => {

            let curIndex = this.state.curIndex;
            let toIndex = curIndex + stepCount;

            if(toIndex > route_setting.details.length - 1) toIndex = route_setting.details.length - 1;

            // 為了防止玩家直接關掉，所以在確定終點的時候就可以先記錄了
            Model.recordData(stepCount, toIndex);

            const _interval = setInterval(() => {
                
                if(curIndex == toIndex) {
                    
                    clearInterval(_interval);

                    resolve("The move is completed.");
                    
                    return;
                }

                curIndex ++;
                
                this.setState({ curIndex });
            
            }, route_setting.moving_duration);
        });
    }

    // 文字跑馬燈
    showResult = () => {

        // 取得每一個文字出現的間隔時間
        const duration = route_setting.per_result_text_duration;

        // 取得當前的文字
        const resultText = Model.getCurResultText();
        const resultTextArr = resultText.split("");

        return new Promise((resolve, reject) => {

            let resultText = "", i = 0;

            const _interval = setInterval(() => {

                if(i == resultTextArr.length) {

                    clearInterval(_interval);
                    
                    resolve("The result display is completed.");

                    return;
                }

                resultText += resultTextArr[i];

                // 文字采逐步顯示，故 setState 不要放在最後的 call back，而是放在這邊
                this.setState({ resultText });

                i++;

            }, duration);

        });
    }

    // 骰骰子
    doDice = () => {

        // 取得骰子點數
        const diceNumber = Math.floor(Math.random() * 6) + 1;
        
        this.setState({
            curDiceNumber : diceNumber,
            resultText : "",
            canClick : false,
        });

        // 先移動
        this.move(diceNumber)
            .then(success => {
                // 接著顯示文字
                return this.showResult();
            })
            .then(success => {
                
                // 全部都結束的時候就開啟所有按鍵
                this.setState({
                    canClick : true
                });

            });
    }

    // 重置遊戲
    reset = () => {
        
        Model.resetData();

        const curIndex = Model.getCurIndex();
        const resultText = Model.getCurResultText();
        const curDiceNumber = Model.getCurDiceNumber();

        this.setState({
            curIndex,
            resultText,
            curDiceNumber
        });
    }

    // 顯示結果清單
    showResultList = () => {
        this.setState({ isShowResultList : true });
    }

    // 隱藏結果清單
    hideResultList = () => {
        this.setState({ isShowResultList : false });
    }

    // 畫出包含外層容器的整個路線元件
    drawRouteWithContainer = () => {

        const details = route_setting.details;

        // 用index的最大差異計算長寬
        let x_min_index = 0, x_max_index = 0, y_min_index = 0, y_max_index = 0;
           
        for(let i = 0, len = details.length; i < len; i++) {

            const detail = details[i];
            const x = detail.x;
            const y = detail.y;

            if(x > x_max_index) x_max_index = x;
            if(y > y_max_index) y_max_index = y;
            if(x < x_min_index) x_min_index = x;
            if(y < y_min_index) y_min_index = y;
        };

        const x_index_range = x_max_index - x_min_index + 1;
        const y_index_range = y_max_index - y_min_index + 1;
        const size = route_setting.size;
        const width = (size * x_index_range);
        const height = (size * y_index_range);

        const style = {
            position : "absolute",
            width : width + "px",
            height : height + "px",
            bottom : 0 + "px",
            left : 50 + "%",
            marginLeft : -(width / 2) + "px",
        };

        return (
            <div className="" style={style}>
                {this.drawRoute()}
            </div>
        );
    }

    // 畫出路線元件
    drawRoute = () => {

        let route = [];

        const details = route_setting.details;
        const size = route_setting.size;
        const standOnColor = route_setting.stand_on_color;
        const startIndex = route_setting.start_index;

        for(let i = 0, len = details.length; i < len; i++)
        {
            const detail = details[i];

            const style_basic = 
            {
                width : size + "px",
                height : size + "px",
                lineHeight : (size - 6) + "px",
                fontSize : 22 + "px",
                fontWeight : "bold",
                textAlign : "center",
                left : detail.x * size + "px",
                bottom : detail.y * size + "px",

                // 起始點以前的都塗黑顯示
                backgroundColor : (i <= startIndex) ? "rgba(0, 0, 0, 0.8)" : detail.color,
                
                position : "absolute",
                fontFamily : 'Oswald',
                boxSizing : "border-box"
            };

            let style_mask = Object.assign({}, style_basic); 
            style_mask.backgroundColor = ( i == this.state.curIndex && i != startIndex) ? standOnColor : null;
            style_mask.left = style_mask.bottom = -3 + "px";

            route.push(
                <div key={i.toString()} className="border" style={style_basic}>
                    <div className="border" style={style_mask}>
                        {i}
                    </div>
                </div>
            );
        };
       
        return route;
    }

    render()
    {
        // 判斷配置檔的合法性
        if(!this.isSettingLegal()) {
            console.error("Const setting illegal.");
            return null;
        };

        // 定義中間區塊的CSS
        const width = 990;
        const height =  (100 + 495 + 35);

        const styleContainer = {
            position : "absolute",
            height : height + "px",
            width : width + "px",
            top : "50%",
            marginTop : -(height / 2) + "px",
            left : "50%",
            marginLeft : -(width / 2) + "px"
        };

        const resultListComp = this.state.isShowResultList ? 
            (<ResultListComp onClickClose={this.hideResultList} />) : null;

        return (
            <div style={{height:"100%", width:"100%"}}>

                <div className="" style={styleContainer}>
                    {/* 路線 */}
                    {this.drawRouteWithContainer()}

                     {/* 上方的結果顯示 */}
                    <ResultPanelComp content={this.state.resultText}/>
                </div>

                {/* 骰子元件 */}
                <DiceComp onClick={this.doDice} diceNumber={this.state.curDiceNumber} canClick={this.state.canClick}/>

                {/* 重置按鈕元件 */}
                <ResetBtnComp onClick={this.reset} canClick={this.state.canClick}/>

                {/* 結果清單顯示按鈕元件 */}
                <ResultListShowBtnComp onClick={this.showResultList} canClick={this.state.canClick} />

                {/* 結果清單 */}
                {resultListComp}
            </div>
        );
    }

}

export default Game;
