// 遊戲狀態 Model

import { route_setting, content_banks } from '../const/const.js';


class AdventureResultRecord
{
    constructor() {

        // 紀錄結果的 index
        this.resultIndex = null;

        // 紀錄結果的內容
        this.resultText = null;

        // 記錄結果的題庫類型(如果沒有則為null)
        this.bankType = null;

        // 紀錄從該題庫類型中所抽到的題目 index
        this.bankIndex = null;
    }
}


class AdventureDataModel
{


    constructor() {

        // 紀錄當前的位置
        this.curIndex = null;

        // 紀錄起始點
        this.startIndex = null;

        // 紀錄當前骰到的所有數字
        this.curDiceNumbers = [];

        // 紀錄當前結果
        this.resultRecords = [];

        // 紀錄已經使用過的題庫 index (content_bank_type : index)
        this.usedBankIndex = {};

        // 紀錄是否已經結束
        this.isEnd = null;
    }

    // 初始化資料
    initData() {

        // 與 localStorage 的資料做比對，檢查一些較為關鍵的資訊，來判斷設定檔是否有被修改
        // 如果已經被修改，則進行初始化，並採用配置檔預設的資料
        const dataFromLocalStorage = JSON.parse(localStorage.getItem("adventureData"));

        // 如果有 localStorage 的資訊
        if(dataFromLocalStorage) {

            // 需要先與配置檔進行比對，檢查一些較為關鍵的資訊，來判斷配置檔是否有被更改
            // 若已經被修改，則初始化，採用配置檔預設的設定

            // 檢查起始點是否有更改
            const startIndex = route_setting.start_index;
            if(dataFromLocalStorage.startIndex != startIndex) {
                
                console.error("Illegal start index ", dataFromLocalStorage.startIndex);
                
                // 將資料恢復成原始設定
                this.resetData();
                
                return;
            }

            // 檢查骰到的結果是否符合當前配置設定檔
            const resultRecords = dataFromLocalStorage.resultRecords;

            for(let i = 0, len = resultRecords.length; i < len; i++){
                const index = resultRecords[i].resultIndex;
                const text = resultRecords[i].resultText;
                const bankType =  resultRecords[i].bankType;

                const compareContentData = route_setting.contents.filter(x => x.index + startIndex == index);

                // 檢查 index
                if(!compareContentData.length) {
                    
                    console.error("Illegal index at ", index, resultRecords);
                    
                    // 將資料恢復成原始設定
                    this.resetData();

                    return;
                }

                // 檢查題庫類型
                // 由於有題庫類型的內容，一定會被加入抽完題庫內題目後的文字。
                // 故只需要檢查是否為同題庫即可，同題庫就算合格
                if(compareContentData[0].type && compareContentData[0].type == bankType) {
                    continue;
                }

                if((bankType || compareContentData[0].type) && compareContentData[0].type != bankType) {

                     console.error("Illegal bank type :", index, bankType, compareContentData[0].type);
                    
                    // 將資料恢復成原始設定
                    this.resetData();

                    return;
                }

                // 最後才檢查文字內容
                if(compareContentData[0].text !== text){

                    console.error("Illegal content " + text + " at ", index, resultRecords);
                    
                    // 將資料恢復成原始設定
                    this.resetData();

                    return;
                }
            }
        }
        // 如果沒有 localStorage 的資訊，則初始化，採用配置檔預設的設定
        else {
            
            console.log("No adventure data in local storage.");

            // 將資料恢復成原始設定
            this.resetData();

            return;
        }

        this.startIndex = dataFromLocalStorage.startIndex;
        this.curIndex = dataFromLocalStorage.curIndex;
        this.curDiceNumbers = dataFromLocalStorage.curDiceNumbers;
        this.resultRecords = dataFromLocalStorage.resultRecords;
        this.usedBankIndexs = dataFromLocalStorage.usedBankIndexs;
        this.isEnd = dataFromLocalStorage.isEnd;

        // 將資料存入 localStorage
        this.recordInLocalStorage();
    }

    // 將資料恢復成原始設定
    resetData() {
        this.startIndex = route_setting.start_index;
        this.curIndex = this.startIndex;
        this.curDiceNumbers = [];
        this.resultRecords = [];
        this.usedBankIndexs = {};
        this.isEnd = false;

        // 將資料存入 localStorage
        this.recordInLocalStorage();
    }

    // 存取資料
    recordData(stepCount, resultIndex) {
 
        if(this.isEnd || resultIndex == this.curIndex) return;

        // 取得起始點
        const startIndex = this.startIndex;

        // 紀錄當前結果
        let resultRecord = new AdventureResultRecord();
        resultRecord.resultIndex = resultIndex;

        const contentData = route_setting.contents.filter(x => x.index + startIndex == resultIndex);
        if(!contentData.length) {
            console.error("Record error: content not exists at index ", resultIndex - startIndex);
            return;
        }

        resultRecord.resultText = contentData[0].text;

        // 判斷是否有題庫類型
        resultRecord.bankType = contentData[0].type;

        if(resultRecord.bankType != null) {

            resultRecord.bankIndex = this.getRandomBankIndex(resultRecord.bankType);

            if(resultRecord.bankIndex != null) {
                resultRecord.resultText += 
                ("：\n" + this.getContentFromBankByIndex(resultRecord.bankType, resultRecord.bankIndex));
            } 
        }
        
        this.resultRecords.push(resultRecord);

        // 紀錄當前骰到的數字
        this.curDiceNumbers.push(stepCount);

        // 紀錄當前的位置
        this.curIndex = resultIndex;
        
        // 紀錄是否已經結束
        this.isEnd = (this.curIndex >= route_setting.details.length - 1);

        // 將資料存入 localStorage
        this.recordInLocalStorage();
    }

    // 將資料存入 localStorage
    recordInLocalStorage() {

        const adventureData = {
            curIndex : this.curIndex,
            startIndex : this.startIndex,
            curDiceNumbers : this.curDiceNumbers,
            resultRecords : this.resultRecords,
            usedBankIndexs : this.usedBankIndexs,
            isEnd : this.isEnd
        };

        // console.log("Record:", adventureData);

        localStorage.setItem("adventureData", JSON.stringify(adventureData));
    }

    // 根據題庫類型取得對應的 index
    getRandomBankIndex(bankType) {

        const contentBank = content_banks[bankType];
        
        if(!contentBank) {
            console.error("No bank type or content bank not exists:", bankType);
            return null;
        }

        if(!contentBank.length) {
            console.error("No content in content bank:", contentBank);
            return null;
        }

        let index = null;

        // 檢查是否已經有記錄過
        const usedContentBank = this.usedBankIndexs[bankType];

        // 沒記錄過就直接隨機產生
        if(!usedContentBank) {
            
            index = Math.floor(Math.random() * contentBank.length);

        // 有記錄過的
        } else {

            // 先把內容都轉成 index，再把用過的 index 給篩選掉
            const theRestIndexUnused = 
                contentBank.map( (x, i) => i ).filter( i => usedContentBank.indexOf(i) == -1);

            if(!theRestIndexUnused || !theRestIndexUnused.length) {
                console.log("No more contents in content bank can be used:", contentBank);
                return null;
            }

            index = theRestIndexUnused[Math.floor(Math.random() * theRestIndexUnused.length)];
        }

        // 將使用過的 bank index 給記錄起來
        if(!this.usedBankIndexs[bankType]) {
            this.usedBankIndexs[bankType] = [];
        } 

        this.usedBankIndexs[bankType].push(index);

        return index;
    }

    // 根據 index 與題庫類型取得對應的題庫內容
    getContentFromBankByIndex(bankType, index) {
        
        const contentBank = content_banks[bankType];
        
        if(!contentBank) {
            console.log("No bank type:", bankType);
            return "";
        }

        return contentBank[index] || "";
    }

    // 判斷遊戲是否已經結束
    isGameOver() {
        return this.isEnd;
    }

    // 取得結果清單
    getResultTexts() {
        return this.resultRecords.map( x => x.resultText);
    }

    // 取得當前的文字內容
    getCurResultText() {
        return (this.resultRecords.length) ? 
            this.resultRecords[this.resultRecords.length - 1].resultText : "";
    }

    // 取得當前的位置
    getCurIndex() {
        return this.curIndex;
    }

    // 取得最新骰到的數字
    getCurDiceNumber() {
        return (this.curDiceNumbers.length) ? this.curDiceNumbers[this.curDiceNumbers.length - 1] : 0;
    }

   
}

const Model = new AdventureDataModel();
export default Model;
