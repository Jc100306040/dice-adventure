// 骰子元件(含骰子數字、骰子按鈕)

import React, { Component } from 'react';
import { route_setting } from '../const/const.js';
import Btn from './common/Btn.js';
import Model from '../model/AdventureDataModel.js';


class DiceComp extends Component
{


    constructor(props)
    {
        super(props);
    }

    // 按鈕點擊
    onClick = () => {
        
        // 防呆
        if(!this.props.canClick) return;

        this.props.onClick && this.props.onClick();
    }

    render()
    {   
        // 骰子按鈕要多檢查遊戲是否已經結束
        const enabled = !Model.isGameOver() ? this.props.canClick : false;

        // 設置CSS
        const size = route_setting.size;
        const width = size;
        const height = size;

        const style_container = {
            position : "absolute",
            height : height + "px",
            top : 30 + "px",
            left : 30 + "px", 
        }

        const style_btn = {
            width : width + "px",
            height : height + "px",
        };

        const style_number = {
            fontWeight : "bold", 
            width : width + "px",
            height : height + "px",
            lineHeight : width + "px",
            fontSize : 40,
            fontFamily : 'Oswald',
            textAlign : "center",
        }

        return (
          <div className="" style={style_container}>
            <div className="float-left" style={style_btn} >
                <Btn onClick={this.onClick} enabled={enabled} backgroundUrl={require("../../pic/dice.png")} />
            </div>
            <div className="float-left" style={style_number}>
                {this.props.diceNumber}
            </div>
          </div>
        );
    }


}

export default DiceComp;
