// 結果清單

import React, { Component } from 'react';
import { route_setting } from '../const/const.js';
import Btn from './common/Btn.js';
import Model from '../model/AdventureDataModel.js';


class ResultListComp extends Component
{


    constructor(props)
    {
        super(props);
    }

    render()
    {   
        const style_container = {
            position : "absolute",
            height : "100%",
            width : "100%",
            backgroundColor : "rgba(15, 15, 15, 0.95)"
        }

        const style_btn = {
            position : "absolute",
            top : 30 + "px",
            right : 30 + "px",
        };

        const width = 1140;
        const height = 580;

        const style_list = {
            position : "absolute",
            width : width + "px",
            height : height + "px",
            left : 50 + "%",
            marginLeft : -(width / 2) + "px",
            top : 50 + "%",
            marginTop : -(height / 2) + "px",
            position : "absolute",
            fontSize : 16 + "px",
            fontWeight : "bold",
            color : "rgba(255, 255, 255, 0.85)",

            // 設定行高
            lineHeight : 26 + "px",

            // 有這個東西才會吃到 \n 換行
            whiteSpace: "pre-line",

            // 左右置中
            textAlign : "center",

            // 上下置中（下面這個需要研究原因...）
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            listStyle: "none",
        };

        let text = "";
    
        // 取得遊戲結束後預設的題目
        const finalTexts = 
            Model.isGameOver() ? route_setting.final_contents : [];

        // 取得當前的結果清單
        let resultTexts = Model.getResultTexts();

        resultTexts = resultTexts.concat(finalTexts);

        for(let i = 0, len = resultTexts.length; i < len; i++) {
            let resultText = resultTexts[i];

            // 清單中，由於空間有限，故單一挑戰的顯示不換行，將 \n 換成空格，並移除所有空格
            resultText = resultText.replace(/\n/g, '');
            resultText = resultText.replace(/ /g, '');

            const index = ( i + 1 >= 10) ? (i + 1).toString() : "0" + (i + 1);

            text += (index + ".　" + resultText + "\n");
        };

        // 如果什麼結果都沒有
        if(!resultTexts.length && !text.length) {
            text = "目前尚無結果，請您先開始您的遊戲，謝謝合作。";
        }

        return (
          <div style={style_container}>
            <div className="" style={style_list} >
                {text}
            </div>
            <div style={style_btn} >
                <Btn onClick={this.props.onClickClose} customIconSize={32} isWhiteBorder={true} backgroundUrl={require("../../pic/close2.png")} />
            </div>
          </div>
        );
    }


}

export default ResultListComp;
