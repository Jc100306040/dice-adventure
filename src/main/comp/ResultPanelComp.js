// 結果顯示面板元件

import React, { Component } from 'react';
import { route_setting } from '../const/const.js';
import Btn from './common/Btn.js';


class ResultPanelComp extends Component
{


    constructor(props)
    {
        super(props);
    }

    render()
    {      
        const width = 990;
        const height = 100;
       
        const style_container = {
            position : "absolute",
            fontSize : 20 + "px",
            fontWeight : "bold",
            color : "rgba(255, 255, 255, 0.8)",
            backgroundColor : "rgba(0, 0, 0, 0.95)",
            whiteSpace: "pre-line",
            textAlign : "center",
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            listStyle: "none",
            width : width + "px",
            height : height + "px",
            top : 0 + "px",
            left : 50 + "%",
            marginLeft : -(width / 2) + "px",
        }

        return (
          <div className="border" style={style_container}>
            {this.props.content}
          </div>
        );
    }


}

export default ResultPanelComp;
