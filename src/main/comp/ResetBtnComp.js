// 重置按鈕

import React, { Component } from 'react';
import { route_setting } from '../const/const.js';
import Btn from './common/Btn.js';


class ResetBtnComp extends Component
{


    constructor(props)
    {
        super(props);
    }

    // 按鈕點擊
    onClick = () => {
        
        // 防呆
        if(!this.props.canClick) return;

        this.props.onClick && this.props.onClick();
    }

    render()
    {   
        const size = route_setting.size;
        const width = size;
        const height = size;

        const style_container = {
            position : "absolute",
            height : height + "px",
            bottom : 30 + "px",
            right : 30 + "px", 
        }

        const style_btn = {
            width : width + "px",
            height : height + "px",
        };

        return (
          <div style={style_container}>
            <div style={style_btn} >
                <Btn onClick={this.onClick} enabled={this.props.canClick} backgroundUrl={require("../../pic/reset.png")} />
            </div>
          </div>
        );
    }


}

export default ResetBtnComp;
