// 按鈕元件

import React, { Component } from 'react';
import { route_setting } from '../../const/const.js';

class Btn extends Component
{


    constructor(props)
    {
        super(props);
    }

    onClickFunction = () => 
    {
        if(this.props.enabled != null && this.props.enabled != undefined && !this.props.enabled) {
            return;
        }
        this.props.onClick && this.props.onClick();
    }

    render()
    {
        const size = route_setting.size;
        const size_icon = this.props.customIconSize || (size - 8);
        const text = this.props.showText? this.props.text : ""; 
        const url = this.props.backgroundUrl;
        const backgroundImage = "url(" + url + ")";

        const style_btn = {
            position : "relative",
            border : (this.props.isWhiteBorder ? "solid 3px white" : "solid 3px black"),
            width : size + "px",
            height : size + "px",
            boxSizing : "border-box"
        };

        const style_icon = {
            position : "absolute",
            width : size_icon + "px",
            height : size_icon + "px",
            backgroundSize : "cover",
            left : 50 + "%",
            marginLeft : -(size_icon/2) + "px",
            top : 50 + "%",
            marginTop : -(size_icon/2) + "px",
            backgroundImage : backgroundImage
        };

        let className = "";

        if((this.props.enabled == null || this.props.enabled == undefined) || this.props.enabled) {
            style_btn["cursor"] = "pointer";
            style_btn["opacity"] = 1;
        } else {
            style_btn["cursor"] = null;
            style_btn["opacity"] = 0.35;
        }

        return (
          <div className={className} style={style_btn} onClick={this.onClickFunction}>
              <div style={style_icon} />
          </div>
        );
    }


}

export default Btn;
