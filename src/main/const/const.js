import React, { Component } from 'react';


// 定義遊戲層種類
const layer_type = 
  	{
      	COVER   : "COVER",
      	GAME    : "GAME"
  	};

// 封面文字
const cover_text = "敢不敢大冒險";

// 題庫類型
const content_bank_type = 
	{
		// 問答題庫(一般型)
		NORMAL_QUESTION : "normal_question"
	};

// 題庫
const content_banks = 
	{	
		// 問答題庫(一般型)
		[content_bank_type.NORMAL_QUESTION] : [
			"說出，異性最讓你心動 / 討厭的3個時候",
			"說出，跟歷任交往對象吵架最兇的3次經歷",
			"「依次」說出，首次見到某異性時會先注意的5個部位",
			"說出在 2022 想達成的3個人生目標",
			"說出，如果你只剩3個月可以活，最想做的3件事",
			"說出，目前為止，認為自己做過的在道德上最糟糕的3件事",
			"說出，不同動漫作品中，最喜歡的3個異性(作品 / 角色 / 原因)",
			"說出，如果人生能夠重來，目前會想回到過去改變的3件事",
			"說出，人生目前為止，讓你感到最快樂的3個故事",
			"說出，目前看過的最愛的5部電影及原因",
			"說出，目前看過的最愛的5部動漫及原因",
			"說出，自己最討厭自己的3個個性及原因",
			"說出，在不考量時間與精力的成本下，最想要突然擁有的3個技能及原因",
			"說出，除手機、錢包、鑰匙外，地震、火災發生時，緊急會拿的前 3 樣東西",
			"說出，近期內最深刻的3件事 / 大學最深刻的3件事",
			"說出，最想告訴後代子女的3件事",
			"說出，碰到害羞的事情2件 / 碰到最狂的事情2件",
			"說出，害怕的事 / 當兵印象最深的3件事",
			"說出，最近是為什麼哭? 如何紓解壓力?",
			"說出，覺得曖昧期到交往前覺得時間要多長？",
			"說出，1個念念不忘的異性緣由 / 名字",
			"說出，你覺得跟異性的底線是什麼? 你遇過覺得最誇張的事?",
			"說出，3個沒告訴過任一位在場者的秘密",
			"說出，自己發生覺得很糗的3件事"
		],
	};

// 冒險路線相關設置
const route_setting = 
	{
		// 長寬尺寸
		size : 55,

		// 路線細節
		details : [
			{ x : 17, y : 0, color : "#000000" },
			{ x : 16, y : 0, color : "#7D7DFF" }, 
			{ x : 15, y : 0, color : "#7D7DFF" }, 
			{ x : 14, y : 0, color : "#7D7DFF" }, 
			{ x : 13, y : 0, color : "#7D7DFF" }, 
			{ x : 12, y : 0, color : "#7D7DFF" }, 
			{ x : 11, y : 0, color : "#7D7DFF" }, 
			{ x : 10, y : 0, color : "#82D900" }, 
			{ x : 9,  y : 0, color : "#82D900" }, 
			{ x : 8,  y : 0, color : "#82D900" }, 
			{ x : 7,  y : 0, color : "#82D900" },
			{ x : 6,  y : 0, color : "#82D900" }, 
			{ x : 5,  y : 0, color : "#82D900" }, 
			{ x : 4,  y : 0, color : "#FF2D2D" }, 
			{ x : 3,  y : 0, color : "#FF2D2D" }, 
			{ x : 2,  y : 0, color : "#FF2D2D" },
			{ x : 1,  y : 0, color : "#FF2D2D" },

			{ x : 0,  y : 0, color : "#FF2D2D" },
			{ x : 0,  y : 1, color : "#FF2D2D" },
			{ x : 0,  y : 2, color : "#9F35FF" },
			{ x : 0,  y : 3, color : "#9F35FF" },
			{ x : 0,  y : 4, color : "#9F35FF" },
			{ x : 0,  y : 5, color : "#9F35FF" },
			{ x : 0,  y : 6, color : "#9F35FF" },
			{ x : 0,  y : 7, color : "#9F35FF" },
			{ x : 0,  y : 8, color : "#FF8F59" },
				
			{ x : 1,  y : 8, color : "#FF8F59" },
			{ x : 2,  y : 8, color : "#FF8F59" },
			{ x : 3,  y : 8, color : "#FF8F59" },
			{ x : 4,  y : 8, color : "#FF8F59" },
			{ x : 5,  y : 8, color : "#FF8F59" },
			{ x : 6,  y : 8, color : "#FFDC35" },
			{ x : 7,  y : 8, color : "#FFDC35" },
			{ x : 8,  y : 8, color : "#FFDC35" },
			{ x : 9,  y : 8, color : "#FFDC35" },
			{ x : 10, y : 8, color : "#FFDC35" },
			{ x : 11, y : 8, color : "#FFDC35" },
			{ x : 12, y : 8, color : "#7D7DFF" },
			{ x : 13, y : 8, color : "#7D7DFF" },
			{ x : 14, y : 8, color : "#7D7DFF" },
			{ x : 15, y : 8, color : "#7D7DFF" },
			{ x : 16, y : 8, color : "#7D7DFF" },
			{ x : 17, y : 8, color : "#7D7DFF" },

			{ x : 17, y : 7, color : "#81C0C0" },
			{ x : 17, y : 6, color : "#81C0C0" },
			{ x : 17, y : 5, color : "#81C0C0" },
			{ x : 17, y : 4, color : "#81C0C0" },
			{ x : 17, y : 3, color : "#81C0C0" },
			{ x : 17, y : 2, color : "#81C0C0" },

			{ x : 16, y : 2, color : "#CA8EC2" },
			{ x : 15, y : 2, color : "#CA8EC2" },
			{ x : 14, y : 2, color : "#CA8EC2" },
			{ x : 13, y : 2, color : "#CA8EC2" },
			{ x : 12, y : 2, color : "#CA8EC2" },
			{ x : 11, y : 2, color : "#CA8EC2" },
			{ x : 10, y : 2, color : "#C48888" },
			{ x : 9,  y : 2, color : "#C48888" },
			{ x : 8,  y : 2, color : "#C48888" },
			{ x : 7,  y : 2, color : "#C48888" },
			{ x : 6,  y : 2, color : "#C48888" },
			{ x : 5,  y : 2, color : "#C48888" },
			{ x : 4,  y : 2, color : "#A5A552" },
			{ x : 3,  y : 2, color : "#A5A552" },
			{ x : 2,  y : 2, color : "#A5A552" },

			{ x : 2,  y : 3, color : "#A5A552" },
			{ x : 2,  y : 4, color : "#A5A552" },
			{ x : 2,  y : 5, color : "#A5A552" },
			{ x : 2,  y : 6, color : "#5CADAD" },

			{ x : 3,  y : 6, color : "#5CADAD" },
			{ x : 4,  y : 6, color : "#5CADAD" },
			{ x : 5,  y : 6, color : "#5CADAD" },
			{ x : 6,  y : 6, color : "#5CADAD" },
			{ x : 7,  y : 6, color : "#5CADAD" },
			{ x : 8,  y : 6, color : "#9999CC" },
			{ x : 9,  y : 6, color : "#9999CC" },
			{ x : 10, y : 6, color : "#9999CC" },
			{ x : 11, y : 6, color : "#9999CC" },
			{ x : 12, y : 6, color : "#9999CC" },
			{ x : 13, y : 6, color : "#9999CC" },
			{ x : 14, y : 6, color : "#0080FF" },
			{ x : 15, y : 6, color : "#0080FF" },

			{ x : 15, y : 5, color : "#0080FF" },
			{ x : 15, y : 4, color : "#0080FF" },

			{ x : 14, y : 4, color : "#0080FF" },
			{ x : 13, y : 4, color : "#0080FF" },
			{ x : 12, y : 4, color : "#BE77FF" },
			{ x : 11, y : 4, color : "#BE77FF" },
			{ x : 10, y : 4, color : "#BE77FF" },
			{ x : 9,  y : 4, color : "#BE77FF" },
			{ x : 8,  y : 4, color : "#BE77FF" },
			{ x : 7,  y : 4, color : "#BE77FF" }
		],

		// 定義內容(索引：文字)
		// 注意，定義檔內的 \n 由於會在結果清單中被拿掉，弄成一行，
		// 故，前面一定要有標點符號！
		contents : [
			{ index : 1,  text : "穿著裙子/泡泡襪/性轉裝"},
			{ index : 2,  text : "去買一件配件，帶整天"},
			{ index : 3,  text : "COS一整天 (1個角色)"},
			{ index : 4,  text : "貼紋身貼紙"},
			{ index : 5,  text : "染頭髮 (顏色由他人指定)"},
			{ index : 6,  text : "化妝一天 (畫眼影跟口紅)"},

			{ index : 7,  text : "在人行道上波比跳20下"},
			{ index : 8,  text : "學一段迷因拍成影片"},
			{ index : 9,  text : "去電子遊樂場與每人開戰，需在每位的對戰中獲勝至少1次"},
			{ index : 10, text : "去天橋算命"},
			{ index : 11, text : "去女僕咖啡廳"},
			{ index : 12, text : "與在場者一同去網咖打英雄聯盟，且必須一直大聲噴隊友"},

			{ index : 13, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 14, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 15, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 16, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 17, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 18, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},

			{ index : 19, text : "請路人喝飲料。 \n 在飲料店或超商附近隨機找尋一位路人，帶他/她去買飲料，\n 由路人自行選擇飲料 (失敗5次，也視同完成挑戰)"},
			{ index : 20, text : "公開 Google 瀏覽紀錄(一週)。\n 由挑戰者自行滑手機讓在場者瀏覽紀錄，滑至時間一週前即可停止"},
			{ index : 21, text : "公開近期 Google 搜尋紀錄(20筆)"},
			{ index : 22, text : "喝一罐番茄汁"},
			{ index : 23, text : "坐在人來人往的路上，喝完一杯飲料"},
			{ index : 24, text : "公開手機相簿(三天內)"},

			{ index : 25, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 26, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 27, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 28, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 29, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 30, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},

			{ index : 31, text : "搭配一套自己沒有穿過的風格，拍照留念"},
			{ index : 32, text : "品嚐蛇肉套餐"},
			{ index : 33, text : "吃納豆(不能加醬油)"},
			{ index : 34, text : "跟大家去「正常到不行」的按摩店按摩，\n 然後順便問掌櫃的：「請問你們這邊有在做黑的嗎？」"},
			{ index : 35, text : "手持「當韓先生來敲門」，並與書合影、發文臉書。\n 內容：「購入新書，當韓先生來敲門！我挺韓，我驕傲，鑽石韓粉來報到！2024，非韓不投！」\n 發文後二十四小時內不得刪文，且需設「公開」"},
			{ index : 36, text : "打給父、母、兄、弟、姐、妹擇一，並說「我愛你」"},

			{ index : 37, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 38, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 39, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 40, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 41, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 42, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},

			{ index : 43, text : "竹葉青一杯，搭配特殊料理"},
			{ index : 44, text : "伏特加一杯，搭配特殊料理"},
			{ index : 45, text : "給開司一罐啤酒！\n 在路邊喝朝日啤酒！並一邊大喊：「給開司一罐啤酒！」"},
			{ index : 46, text : "月桂冠一杯，搭配特殊料理"},
			{ index : 47, text : "威士忌一杯，搭配特殊料理"},
			{ index : 48, text : "自選酒精飲料，搭配特殊料理"},

			{ index : 49, text : "獲得禮物：\n 精裝神奇果實乙盒"},
			{ index : 50, text : "獲得禮物：\n 500 元刮刮樂乙張"},
			{ index : 51, text : "獲得禮物：\n 「當韓先生來敲門」乙本"},
			{ index : 52, text : "獲得禮物：\n 熊掌"},
			{ index : 53, text : "獲得禮物：\n 2000 元內 PS4 遊戲，遊戲可指定"},
			{ index : 54, text : "獲得禮物：\n 純白地獄"},

			{ index : 55, text : "說出，在場者中最願意交往的對象及原因 (可支援性轉)"},
			{ index : 56, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 57, text : "說出，在場者中最不願意交往的對象及原因 (可支援性轉)"},
			{ index : 58, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 59, text : "說出，給在場每一位的認真建議1條"},
			{ index : 60, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			
			{ index : 61, text : "說出，在場誰跟你相處最合得來、最合不來，並告知原因"},
			{ index : 62, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 63, text : "說出，在場的每一位你分別討厭的地方1點 / 自己最棒的3個點"},
			{ index : 64, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 65, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
			{ index : 66, type : content_bank_type.NORMAL_QUESTION, text : "問答題庫(一般型)抽1"},
		],

		// 遊戲結束後附加的題目
		final_contents : [
			"說出，想告訴大家的話"
		],

		// 移動的間隔時間(ms)
		// moving_duration : 400,
		moving_duration : 100,

		// 文字完整顯示所需時間(ms)
		result_show_duration : 1400,

		// 每一個結果文字出現的間隔時間(ms)
		// per_result_text_duration : 100,
		per_result_text_duration : 10,

		// 所在位置的顏色顯示
		stand_on_color : "rgba(255, 255, 255, 0.7)", 

		// 定義起始點
		start_index : 24
	};

export { layer_type, cover_text, content_bank_type, content_banks, route_setting};