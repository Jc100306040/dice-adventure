// 主要介面，用以控制所有層的變化

import React, { Component } from 'react';
import { layer_type } from './const/const.js';
import Cover from './Cover.js';
import Game from './Game.js';


class Main extends Component 
{

    constructor(props)
    {
        super(props);

        // 設置 Call Back Refs 
        this.node_layer = null;
        this.set_node_layer = ele => { this.node_layer = ele };

        this.state = 
            {
                // 層種類，預設為封面
                layer_type : layer_type.COVER,

            }; 
    }

    // 進入遊戲
    enterGame = () =>
    {
        this.setState({
            layer_type : (this.state.layer_type != layer_type.GAME) ? layer_type.GAME : layer_type.COVER,
        });    
    }

    render()
    {
        const height = window.innerHeight;
        const width = window.inner;

        // 封面層
        const cover_layer = (this.state.layer_type == layer_type.COVER)? <Cover cbFunc={this.enterGame} /> : null;

        // 遊戲層
        const game_layer = (this.state.layer_type == layer_type.GAME)? <Game /> : null;

        return (
            <div className="" ref={this.set_node_layer} style={{height:"100vh", width:width, position:"relative"}}>
                {cover_layer}
                {game_layer}
            </div>
        );
    }

}

export default Main;
